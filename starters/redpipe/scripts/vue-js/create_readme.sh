#!/bin/sh
# --- generate README.md ---
cat > $1$2/README.md << EOF
# $2

This is a RedPipe skeleton project with:

## Front components

- Vue.js
- Axios
- Font Awesome
- Bulma css
- Open Sans font

# Run it

- Type this: \`mvn install exec:java\` 
- or if you want to change http port: \`PORT=8080 mvn install exec:java\`

EOF
