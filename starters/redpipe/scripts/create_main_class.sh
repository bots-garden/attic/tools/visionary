#!/bin/sh
# --- generate Main.java ---
mkdir -p $1$2/src/main/java/${3//./\/}
cat > $1$2/src/main/java/${3//./\/}/Main.java << EOF
package $3;

import net.redpipe.engine.core.Server;
import io.vertx.core.json.JsonObject;
import java.util.Optional;

public class Main {
  public static void main(String[] args) {
    JsonObject config = new JsonObject();
    Integer httport = Integer.parseInt(Optional.ofNullable(System.getenv("PORT")).orElse("$4"));
    config.put("http_port", httport);

    new Server().start(config, StaticResource.class, HelloResource.class)
      .subscribe(
        v -> System.out.println("RedPipe server is started"),
          Throwable::printStackTrace
      );
  }
}
EOF

