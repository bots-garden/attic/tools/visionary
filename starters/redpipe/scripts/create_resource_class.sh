#!/bin/sh
# --- generate RedPipe Resource ---
echo "$1" # current rep
echo "$2" # Class name
echo "$3" # Package
echo "$4" # Route
echo "$5" # Method

mkdir -p $1/src/main/java/${3//./\/}
cat > $1/src/main/java/${3//./\/}/$2.java << EOF
package $3;

import io.vertx.core.json.JsonObject;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("$4")
public class $2 {
  /**
    * Test it:
    * curl -X GET http://localhost:8080$4
    */
  @Produces("application/json; charset=utf-8")
  @GET
  public JsonObject $5() {
    return new JsonObject().put("message", "Hello World!");
  }

  /**
    * Test it: 
    * curl -H "Content-Type: application/json" -X POST -d '{"who":"Bob Morane"}' http://localhost:8080$4
    */
  @Produces("application/json; charset=utf-8")
  @POST
  public JsonObject $5(JsonObject data) {
    return new JsonObject().put("message","hello " + data.getString("who"));
  }
}
EOF


