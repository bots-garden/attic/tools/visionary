#!/bin/sh
# --- generate HelloResource.java ---
mkdir -p $1$2/src/main/java/${3//./\/}
cat > $1$2/src/main/java/${3//./\/}/HelloResource.java << EOF
package $3;

import io.vertx.core.json.JsonObject;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/hello")
public class HelloResource {
  /**
    * Test it:
    * curl -X GET http://localhost:$4/hello
    */
  @Produces("application/json; charset=utf-8")
  @GET
  public JsonObject hello() {
    return new JsonObject().put("message", "Hello World!");
  }

  /**
    * Test it: 
    * curl -H "Content-Type: application/json" -X POST -d '{"who":"Bob Morane"}' http://localhost:$4/hello
    */
  @Produces("application/json; charset=utf-8")
  @POST
  public JsonObject hello(JsonObject data) {
    return new JsonObject().put("message","hello " + data.getString("who"));
  }
}
EOF

