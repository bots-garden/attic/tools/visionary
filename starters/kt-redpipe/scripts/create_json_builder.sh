#!/bin/sh
# --- generate main.kt ---
mkdir -p $1$2/src/main/kotlin/${3//./\/}
cat > $1$2/src/main/kotlin/${3//./\/}/JsonObjectBuilder.kt << EOF
package $3

import io.vertx.core.json.JsonObject

class JsonObjectBuilder {
  val json = JsonObject()

  infix fun <B> String.to(other: B) {
    json.put(this, other)
  }
}
EOF

cat > $1$2/src/main/kotlin/${3//./\/}/json.kt << EOF
package $3

fun json(init: JsonObjectBuilder.() -> Unit) = JsonObjectBuilder().apply(init).json

EOF


