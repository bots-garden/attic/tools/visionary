#!/bin/sh
# --- generate README.md ---
cat > $1$2/run.sh << EOF
#!/bin/sh
mvn install exec:java -e
EOF
chmod +x $1$2/run.sh
