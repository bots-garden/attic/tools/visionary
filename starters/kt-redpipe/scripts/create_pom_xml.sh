#!/bin/sh
# --- generate pom.xml ---
cat > $1$2/pom.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>$3</groupId>
    <artifactId>$2</artifactId>
    <name>$2</name>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <redpipe.version>$4</redpipe.version>
        <mainClass>$3.MainKt</mainClass>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <kotlin.version>$5</kotlin.version>
        <kotlin.compiler.incremental>true</kotlin.compiler.incremental>
        <arrow.version>$6</arrow.version>
    </properties>

    <build>
        <sourceDirectory>\${project.basedir}/src/main/kotlin</sourceDirectory>
        <testSourceDirectory>\${project.basedir}/src/test/kotlin</testSourceDirectory>

        <plugins>

            <!-- Kotlin -->

            <plugin>
                <artifactId>kotlin-maven-plugin</artifactId>
                <groupId>org.jetbrains.kotlin</groupId>
                <version>\${kotlin.version}</version>

                <configuration>
                    <experimentalCoroutines>enable</experimentalCoroutines>
                </configuration>

                <executions>
                    <execution>
                        <id>compile</id>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>

                    <execution>
                        <id>test-compile</id>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>1.6.0</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>java</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <mainClass>\${mainClass}</mainClass>
                </configuration>
            </plugin>


        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>net.redpipe</groupId>
            <artifactId>redpipe-engine</artifactId>
            <version>\${redpipe.version}</version>
        </dependency>

        <!-- Kotlin -->

        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
            <version>\${kotlin.version}</version>
        </dependency>

        <!-- Arrow -->
        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-core</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-syntax</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-typeclasses</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-data</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-instances-core</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-instances-data</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

        <dependency>
            <groupId>io.arrow-kt</groupId>
            <artifactId>arrow-annotations-processor</artifactId>
            <version>\${arrow.version}</version>
        </dependency>

    </dependencies>

</project>

EOF
