#!/bin/sh
# --- generate main.kt ---
mkdir -p $1$2/src/main/kotlin/${3//./\/}
cat > $1$2/src/main/kotlin/${3//./\/}/main.kt << EOF
package $3

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import net.redpipe.engine.core.Server

fun main(args: Array<String>) {

  val envHttpPort = Option.fromNullable(System.getenv("PORT"))

  val httpPort: Int = when(envHttpPort) {
    is None -> $4
    is Some -> Integer.parseInt(envHttpPort.t)
  }

  val config = json { "http_port" to httpPort }

  Server()
    .start(config,
      StaticResource::class.java,
      HelloResource::class.java
    )
    .subscribe(
      { v -> println("RedPipe server is started") },
      { it.printStackTrace() }
    )
}
EOF


