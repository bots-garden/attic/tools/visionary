# Visionary

**Visionary** is a tool (CLI) to generates [RedPipe](http://redpipe.net/) application skeletons.

## Setup

```shell
git clone git@gitlab.com:bots.garden/tools/visionary.git
```
> OSX
```shell
export VISIONARY=~/<PATH_TO_THE_PROJECT>/visionary
export PATH=$VISIONARY/bin/osx:$PATH
```

> Linux
```shell
export VISIONARY=~/<PATH_TO_THE_PROJECT>/visionary
export PATH=$VISIONARY/bin/linux:$PATH
```

## Generate projects

> Generate a simple Java RedPipe project, then run
```shell
redpipe-simple demo garden.bots 8080 # params: application name, package, http port
cd demo
./run.sh
```

> Generate a Java RedPipe project + statics assets (Vue.js, Axios, Font avesome, Bulma css), then run
```shell
redpipe-vue demo garden.bots 8080 # params: application name, package, http port
cd demo
./run.sh
```

> Generate a simple Kotlin RedPipe project, then run
```shell
kt-redpipe-simple demo garden.bots 8080 # params: application name, package, http port
cd demo
./run.sh
```

## Build the CLI

```shell
pkg visionary.js
```
