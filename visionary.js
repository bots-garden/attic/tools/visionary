const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');
const shell = require('shelljs');

let template = function(tpl, args) {
  var keys = Object.keys(args), fn = new Function(...keys, 'return `' + tpl.replace(/`/g, '\\`') + '`');
  return fn(...keys.map(x => args[x]));
};


try {
  let yamlFile = process.argv[2]
  console.log("Parsing:", yamlFile)
  const config = yaml.safeLoad(fs.readFileSync(yamlFile, 'utf8'));
  const indentedJson = JSON.stringify(config, null, 2);
  
  //console.log(indentedJson);

  /**
   * Project Variables
   *   where: ${arguments.where}
   *   name: ${arguments.name}
   *   package: ${arguments.package}
   *   port: ${arguments.port}
   */

  /*
  config.application.where = template(config.application.where,{arguments:arguments_app})
  config.application.name = template(config.application.name,{arguments:arguments_app})
  config.application.package = template(config.application.package,{arguments:arguments_app})
  config.application.port = template(config.application.port,{arguments:arguments_app})
  */
  /*
  config.application = {
    where: process.argv[3],
    name: process.argv[4],
    package: process.argv[5],
    port: process.argv[6]
  }
  */
  /**
   * Parse arguments
   */
  for(let argument in config.arguments) {
    //console.log(argument, config.arguments[argument])
    config.arguments[argument] = process.argv[config.arguments[argument]]
  }

  // where is the visionary shell
  config.starters = {
    current: __dirname, // path.dirname(process.argv[1]), path of visionary script
    path: path.dirname(process.argv[2]) // path of the used yaml file
  }
  //config.starters.current = template(config.starters.current,{starters:starters})
  //config.starters.path = template(config.starters.path,{starters:starters})

  //console.log("👋 Generating:", config.application.name)

  //console.log(config.application)
  /**
   * Intializing the project
   */
  config.env = process.env

  config.initialize.script.forEach(line => {
    const { stdout, stderr, code } = shell.exec(
      template(line,{
        config: config, 
        application: config.application, 
        arguments: config.arguments, 
        starters: config.starters,
        env: config.env,
        self: config.initialize
      })
      , {silent:true}
    ) // shell.exec
    if(stderr) console.error(stderr, code)
    if(stdout) console.log(stdout)
  })

  /**
   * Front dependencies
   */
  for(let dependency in config.front_dependencies) {
    
    config.front_dependencies[dependency].script.forEach(line => {
      console.log("📦 frontend dependency -> ", dependency)
      const { stdout, stderr, code } = shell.exec(
        template(line,{
          config: config, 
          application: config.application, 
          arguments: config.arguments, 
          starters: config.starters,
          env: config.env,
          self: config.front_dependencies[dependency]
        })
        , {silent:true}
      ) // shell.exec
      if(stderr) console.error(stderr, code)
      if(stdout) console.log(stdout)
    })
  }

  /**
   * Backend dependencies
   */
  for(let dependency in config.backend_dependencies) {
    
    config.backend_dependencies[dependency].script.forEach(line => {
      console.log("📦 backend dependency -> ", dependency)
      const { stdout, stderr, code } = shell.exec(
        template(line,{
          config: config, 
          application: config.application, 
          arguments: config.arguments, 
          starters: config.starters,
          env: config.env,
          self: config.backend_dependencies[dependency]
        })
        , {silent:true}
      ) // shell.exec
      if(stderr) console.error(stderr, code)
      if(stdout) console.log(stdout)
    })
  }

  /**
   * Generators
   */
  for(let dependency in config.frontend_generators) {
    
    config.frontend_generators[dependency].script.forEach(line => {
      console.log("🛠 frontend generator -> ", dependency)
      const { stdout, stderr, code } = shell.exec(
        template(line,{
          config: config, 
          application: config.application, 
          arguments: config.arguments, 
          starters: config.starters,
          env: config.env,
          self: config.frontend_generators[dependency]
        })
        , {silent:true}
      ) // shell.exec
      if(stderr) console.error(stderr, code)
      if(stdout) console.log(stdout)
    })
  }

  for(let dependency in config.backend_generators) {
    
    config.backend_generators[dependency].script.forEach(line => {
      console.log("🛠 backend generator -> ", dependency)
      const { stdout, stderr, code } = shell.exec(
        template(line,{
          config: config, 
          application: config.application,
          arguments: config.arguments,  
          starters: config.starters,
          env: config.env,
          self: config.backend_generators[dependency]
        })
        , {silent:true}
      ) // shell.exec
      if(stderr) console.error(stderr, code)
      if(stdout) console.log(stdout)
    })
  }
  
  for(let dependency in config.others_generators) {
    
    config.others_generators[dependency].script.forEach(line => {
      console.log("🛠 other generator -> ", dependency)
      const { stdout, stderr, code } = shell.exec(
        template(line,{
          config: config, 
          application: config.application,
          arguments: config.arguments, 
          starters: config.starters, 
          env: config.env,
          self: config.others_generators[dependency]
        })
        , {silent:true}
      ) // shell.exec
      if(stderr) console.error(stderr, code)
      if(stdout) console.log(stdout)
    })
  }

} catch (e) {
  console.log(e);
}

